#!/bin/bash

# Script to generate benchmark modular networks with LFR method
# Teo Victor
# 2019-07-02

# bynary_networks dir
b_dir=~/binary_networks

# Output dir
o_dir=~/workspace

#Networks with changing mu
#for i in 0.05 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5; do
  #mkdir $o_dir/LFRmu/$i
  #for s in {1..10}; do
     #$b_dir/benchmark -N 1000 -mu $i -k 4 -maxk 15
	 #mkdir $o_dir/LFRmu/$i/$s
	 #cp network.dat $o_dir/LFRmu/$i/$s
	 #rm time_seed.dat
	 #echo $RANDOM >> time_seed.dat
  #done
#done

#Networks with changing k
for i in 4 5 6 7 8 9 10; do
  mkdir $o_dir/LFRk/$i
  for s in {1..10}; do
	 $b_dir/benchmark -N 1000 -mu 0.25 -k $i -maxk 20
	 mkdir $o_dir/LFRk/$i/$s
	 cp network.dat $o_dir/LFRk/$i/$s
	 rm time_seed.dat
	 echo $RANDOM >> time_seed.dat
  done
done
