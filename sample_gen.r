source("opinion_lib.R")

# Function: sample_gen
# Geracao de amostras
# data_v:         variavel da amostra
# stype:          tipo de amostra (d, n, mean, k, mu ou asch)
# s_size:         tamanho da amostra
# s_dir:          diretorio raiz dos resultados
# n:              tamanho da rede
# m:              tipo da rede
# D:              paramentro d do metodo DW
# timefactor:     fator multiplicador do intervalo temporal
# datad:          diretorio fonte dos dados de redes modulares (apenas para stype k ou mu)
# netfile:        arquivo de rede (apenas para stype Real)
# netname:        nome da rede (apenas para stype Real)
# distform:       forma da distribuicao, normal ou uniforme
# aschp:          aplicacao (ou nao) do parametro de Asch
# aschf:          valor do parametro de asch
# ds:             parametro fixo de rede LFR para stype d
sample_gen <- function(data_v, stype, s_size=3, s_dir="resultados", n=1000, D=0.5, m="er", 
                        timefactor=500, netfile="", netname="", fnettype="edgelist", datad="", distform="unif", 
                        aschp=FALSE, aschf=0.6, ds=NULL, tolerants=200) {
  if (stype == "d" & is.null(ds))
    cat("For d type sample parameter ds must be specified.")
  
  if(!dir.exists(s_dir))
    dir.create(s_dir)
  
  m_dir = paste(s_dir, paste(m, n, sep="_"), sep="/")
  if(!dir.exists(m_dir))
    dir.create(m_dir)
  
  if (!is.null(ds)) {
    m_dir <- paste(m_dir, ds, sep="/")
    if(!dir.exists(m_dir))
      dir.create(m_dir)
  }
  
  if (stype=="Real") network <- load_network(netfile, name=netname, type=fnettype)
  else if (m != "lfr" & stype != "n") network <- create_network(model=m, N=n)#, root_dir = r_dir)
  
  c <- 0
  for (i in data_v) {
    w_dir = paste(m_dir, i, sep="/")
    if(!dir.exists(w_dir))
      dir.create(w_dir)
    
    j <- 1
    total <- length(data_v)*s_size
    
    while (j <= s_size) {
      c <- c+1
      cat(paste("\nObservation", paste(c, total, sep="/"), "initiated."), sep=" ")
      r_dir <- paste(w_dir, j, sep="/")
      if(!dir.exists(r_dir))
        dir.create(r_dir)
      
      if (m == "lfr")
        if (datad == "") network <- create_network(model="lfr", N=n, root_dir = r_dir, datasource=i, datadir=datad, nlfrsample=0)
        else if (stype == "d") network <- create_network(model="lfr", N=n, root_dir = r_dir, datasource=ds, datadir=datad, nlfrsample=j)
        else if (stype == "tol_var") network <- create_network(model="lfr", N=n, root_dir = r_dir, datasource=ds, datadir=datad, nlfrsample=j)
        else network <- create_network(model="lfr", N=n, root_dir = r_dir, datasource=i, datadir=datad, nlfrsample=j)
      else if (stype=="n")
        network = create_network(model=m, N=i, root_dir = r_dir)
        
        
      if (stype=="mean")
        opinion_dynamics(g=network, d=D, tmax=n*(timefactor/D), imin=0, imax=1, root_dir=r_dir, 
                         mean_d=i, dist=distform, asch=aschp, p_asch=aschf)
      else if (stype=="d") 
        opinion_dynamics(g=network, d=i, tmax=n*(timefactor/i), imin=0, imax=1, 
                         root_dir=r_dir, dist=distform, asch=aschp, p_asch=aschf)
      else if (stype=="n")
        opinion_dynamics(g=network, d=D, tmax=i*(timefactor/D), imin=0, imax=1, root_dir=r_dir, 
                         dist=distform, asch=aschp, p_asch=aschf)
      else if (stype=="k" || stype=="mu")
        opinion_dynamics(g=network, d=D, tmax=n*(timefactor/D), imin=0, imax=1, root_dir=r_dir, 
                         dist=distform, asch=aschp, p_asch=aschf)
      else if (stype=="asch")
        opinion_dynamics(g=network, d=D, tmax=n*(timefactor/D), imin=0, imax=1, root_dir=r_dir, 
                         dist=distform, asch=TRUE, p_asch=i)
      else if (stype=="tol")
        opinion_dynamics(g=network, d=D, tmax=n*(timefactor/D), imin=0, imax=1, root_dir=r_dir, 
                         dist=distform, asch=FALSE, tolerant_agents = tolerants)
      else if (stype=="tol_var")
        opinion_dynamics(g=network, d=D, tmax=n*(timefactor/D), imin=0, imax=1, root_dir=r_dir, 
                         dist=distform, asch=FALSE, tolerant_agents = i)
      j <- j+1
    }
  }
}


# Function: sample_asch
# Geracao de amostras utilizando o parametro de asch
# data_v:         variavel da amostra
# s_size:         tamanho da amostra
# s_dir:          diretorio raiz dos resultados
# n:              tamanho da rede
# D:              paramentro d do metodo DW
# m:              tipo da rede
# timefactor:     fator multiplicador do intervalo temporal
# datad:          diretorio fonte dos dados de redes modulares (apenas para stype="none")
# stype:          tipo de amostra (d ou none)
# aschf:          fator de Asch, quando este nao e a variavel
sample_asch <- function(data_v, s_size=1, s_dir="resultados", n=1000, 
                           D=0.5, m="er", timefactor=500, datad="", stype="none", aschf=0.7, distform="unif") {
  if(!dir.exists(s_dir))
    dir.create(s_dir)
  
  if (stype == "d") {
    s_dir = paste(s_dir, paste(m, n, sep="_"), sep="/")
    if(!dir.exists(s_dir))
      dir.create(s_dir)
  }
  
  total <- length(data_v)*s_size
  c <- 0
  
  for (i in data_v) {
    w_dir = paste(s_dir, i, sep="/")
    if(!dir.exists(w_dir))
      dir.create(w_dir)
    
    j <- 1
    while (j <= s_size) {
      c <- c+1
      cat(paste("\n\nObservation", paste(c, total, sep="/"), "initiated."), sep=" ")
      j_dir <- paste(w_dir, j, sep="/")
      if(!dir.exists(j_dir))
        dir.create(j_dir)
      
      if (stype=="d") {
        network = create_network(model=m, N=n, root_dir = j_dir)
        opinion_dynamics(g=network, d=D, tmax=n*(timefactor/D), imin=0, imax=1, root_dir=j_dir,
                         asch=TRUE, p_asch=i, dist=distform)
      }
      else {
        network = create_network(model="lfr", N=n, root_dir = j_dir, datasource=i, datadir=datad, nlfrsample=j)
        opinion_dynamics(g=network, d=D, tmax=n*(timefactor/D), imin=0, imax=1, root_dir=j_dir,
                         asch=TRUE, p_asch=aschf, dist=distform)
      }
      
      j <- j+1
    } # while
    
  } #for
}

# Function: sample_real
# Processo de dinamica de opiniao para redes reais
sample_real <- function(data_v, net_file, out_dir, s_type, filetype="edgelist", net_name="network",
                        s_size=10, asch_f=0, omin=0, omax=1, mean=0.5, distform="unif", D=0.5, 
                        timefactor=5000) {
  
  if(!dir.exists(out_dir))
    dir.create(out_dir)
  
  aschp <- FALSE
  if (asch_f > 0) aschp <- TRUE
  
  net_dir <- paste(out_dir, net_name, sep="/")
  
  if(!dir.exists(net_dir))
    dir.create(net_dir)
  
  network <- load_network(net_file, filetype, name=net_name)
  n <- length(V(network))
  
  total <- length(data_v)*s_size
  c <- 0
  
  for (i in data_v) {
    w_dir <- paste(net_dir, i, sep="/")
    if(!dir.exists(w_dir))
      dir.create(w_dir)
    
    sample <- 1:s_size
    for (j in sample) {
      c <- c+1
      cat(paste("\nObservation", paste(c, total, sep="/"), "initiated."), sep=" ")
      j_dir <- paste(w_dir, j, sep="/")
      if(!dir.exists(j_dir))
        dir.create(j_dir)
      
      if (s_type == "d")
        opinion_dynamics(g=network, d=i, tmax=n*(timefactor/0.5), imin=omin, imax=omax, root_dir=j_dir, 
                         asch=aschp, dist=distform, p_asch=asch_f)
      else if (s_type == "asch")
        opinion_dynamics(g=network, d=D, tmax=n*(timefactor/0.5), imin=omin, imax=omax, root_dir=j_dir, 
                         asch=TRUE, dist=distform, p_asch=i)
      else 
        opinion_dynamics(g=network, d=D, tmax=n*(timefactor/D), imin=omin, imax=omax, root_dir=j_dir, asch=aschp)
    }
  }
}
  
