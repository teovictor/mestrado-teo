############
## clustering_opinions.r
##
## Author:          Teo Victor
## Creation date:   2019-06-07
## 
## Set of functions to find the number of opinion clusters in a given graph

# Function: load_opinions
# Recupera opinioes finais de uma rede no arquivo dist.dat, apos execucao
# do metodo D-W
# opfile:       arquivo com os dados
# N:            tamanho da rede
load_opinions <- function(opfile, N) {
  
  datafile <- read.table(opfile)
  
  all_opinions <- datafile[,2]
  size <- length(all_opinions)
  last_opinions <- all_opinions[(size-N):size]
  
  return(last_opinions)
}

# Function: load_network
# Carrega dados de redes salvas em arquivo
# net_file:       Arquivo com dados da rede
# short_name:     Nome curto da rede
# long_name:      Nome/Descricao da rede
load_network <- function(net_file, name="", type="edgelist"){
  if (type == "edgelist") {
    raw_data <- read.table(net_file)
    net <- graph.data.frame(raw_data, directed = FALSE)
  }
  else if (type == "gml") {
    net <- read_graph(net_file, format=type)
  }
  else if (type == "adjacency") {
    csv_data <- read.csv(net_file, sep=",")
    net <- graph_from_adjacency_matrix(as.matrix(csv_data), mode="undirected", weighted=NULL, diag=FALSE)
  }
  
  net <- as.undirected(net, mode = "collapse")
  
  V(net)$name <- name
  V(net)$ntype <- "Real"
  
  return(net)
}

# Function: find_clusters
# Identifica o numero de clusters de opinioes em uma rede
# o:            vetor de opinioes
# np:           numero de particoes
# min e max:    limites da faixa de distribuicao das opinioes
find_clusters <- function(o, np=100, min=0.0, max=1.0) {
  p <- seq(min, max, length=np+1)
  min_clus_size <- length(o)/500
  if (min_clus_size < 1) min_clus_size <- 1
  
  splits <- 1:np
  for (i in 1:np) {
    splits[i] <- length(o[o > p[i] & o < p[i+1]])
  }
  
  # Inicializacao dos dados
  clusters_count <- 0
  incluster <- FALSE
  size_clusters <- rep(0, np)
  width_clusters <- rep(0, np)
  mean_clusters <- rep(0, np)
  max_split <- max(splits)
  
  c <- 1
  j <- 1
  
  initial_w <- 0
  final_w <- 0
  width_half_big <- 0
  max_split_pass <- FALSE
  init_width <- TRUE
  bigger <- 0
  for (s in splits) {
    if (s > min_clus_size) {
      if (incluster == FALSE) initial_w <- min(o[o > p[c] & o < p[c+1]])
      if (s > bigger) {
        bigger <- s
        mean_clusters[j] <- p[c]
      }
      size_clusters[j] <- size_clusters[j] + s
      incluster <- TRUE
    }
    
    # Para largura do maior cluster a meia altura
    if (s > max_split/2 & init_width == TRUE) {
      if (width_half_big == 0)
        width_half_big <- min(o[o > p[c] & o < p[c+1]])
      else width_half_big <- width_half_big + max(o[o > p[c] & o < p[c+1]])
    }
    if (s == max_split) max_split_pass == TRUE
    
    if (incluster == TRUE & s <= min_clus_size) {
      bigger <- 0
      clusters_count <- clusters_count+1
      incluster <- FALSE
      final_w <- max(o[o < p[c+1]])
      width_clusters[j] <- abs(final_w - initial_w)
      if (max_split_pass == FALSE) width_half_big <- 0
      else init_width <- FALSE
      j <- j+1
    }
    
    c <- c+1
  }
  
  if (incluster == TRUE) {
    clusters_count <- clusters_count+1
    final_w <- max(o[o < p[np]])
    width_clusters[j-1] <- abs(final_w - initial_w)
  }
  
  size_clusters <- size_clusters[which(size_clusters > 0)]
  width_clusters <- width_clusters[which(width_clusters > 0)]
  mean_clusters <- mean_clusters[which(mean_clusters > 0)]
  
  min <- 0
  maxc <- which(size_clusters==max(size_clusters))
  for (m in mean_clusters) {
    #if (abs(mean_clusters[maxc] - m) > min) min <- m
  }
  
  r <- list()
  r$ccount <- clusters_count
  r$pcount <- splits
  r$splits <- p
  r$maxdist <- min
  r$bigwidth <- width_clusters[maxc]
  r$max_size <- max(size_clusters)
  r$sizes <- size_clusters
  r$widths <- width_clusters
  r$means <- mean_clusters
  r$width_big_half <- width_half_big
  
  return(r)
}

cluster_analysis <- function(root_dir, data_v, N=0, s_size=3) {
  n = N
  
  
  resfile <- paste(root_dir,"cluster_analysis_complete.dat", sep="/")
  resfile_c <- paste(root_dir,"cluster_analysis_distribution.dat", sep="/")
  if (file.exists(resfile)) file.remove(resfile)
  if (file.exists(resfile_c)) file.remove(resfile_c)
  
  write.table(as.matrix(t(c("i", "count", "maxsize", "wbig_half", "maxddist", "error"))), 
              file=resfile, 
              row.names = FALSE, col.names = FALSE, append=TRUE)
  
  for (i in data_v) {
    count <-0
    maxs <- 0
    wbig_half <- 0
    maxd <- 0
    
    wdir <- paste(root_dir, i, sep="/")
    
    values <- c()
    for (j in 1:s_size) {
      jdir <- paste(wdir, j, sep="/")
      
      if (N==0) {
        g <- load_network(paste(jdir, "network.dat", sep='/'))
        n = length((V(g)))
      }
      
      opinions <- load_opinions(paste(jdir, "dist.dat", sep='/'), n)
      a_clus <- find_clusters(opinions)
      values <- append(values, a_clus)
      
      count <- count + a_clus$ccount
      maxs <- maxs + a_clus$max_size
      wbig_half <- wbig_half + a_clus$bigwidth
      maxd <- maxd + a_clus$maxdist
      
      write.table(as.matrix(t(c(i, j, a_clus$ccount, a_clus$max_size, a_clus$bigwidth, a_clus$maxdist))), 
                  file=resfile_c, 
                  row.names = FALSE, col.names = FALSE, append=TRUE)
    }
    
    count <- count/s_size
    maxs <- maxs/s_size
    wbig_half <- wbig_half/s_size
    maxd <- maxd/s_size
    error <- sd(values)/sqrt(s_size)
    
    write.table(as.matrix(t(c(i, count, maxs, wbig_half, maxd, error))), 
                file=resfile, 
                row.names = FALSE, col.names = FALSE, append=TRUE)
  }
}

# Function: cluster_count
# Retorna o numero de clusters de opiniao num vetor de opinioes
# root_dir:     diretorio dos resultados
# s_size:       numero de amostras no diretorio de resultados
# model:        tipo de rede dos resultados
# N:            tamanho da rede nos resultados
cluster_count <- function(root_dir="resultados", s_size=3, model="lattice", N=1000) {
  
  m_dir <- paste(root_dir, paste(model, N, sep="_"), sep="/")
  mfile <- paste(m_dir,"clusters.dat", sep="/")
  
  for (D in seq(0.1, 0.75, by=0.05)) {
    w_dir = paste(m_dir, D, sep="/")
    
    j <- 1
    
    count <- 0
    while (j <= s_size) {
      r_dir <- paste(w_dir, j, sep="/")
      opfile <- paste(r_dir, "dist.dat", sep="/")
      opinions <- load_opinions(opfile, N)
      clusters <- find_clusters(opinions)
      #print(find_clusters(opinions))
      
      count <- count + clusters$ccount
      j <- j+1
    }
    
    if (count == 0)  {
      count <- N
    }
    else count <- round(count/s_size)
    
    write.table(as.matrix(t(c(D, count))), 
                file=mfile, 
                row.names = FALSE, col.names = FALSE, append=TRUE)
  }

}

# Function: cluster_count
# Retorna a opiniao media do maior cluster de opiniao de uma rede
# root_dir:     diretorio dos resultados
# s_size:       numero de amostras no diretorio de resultados
# model:        tipo de rede dos resultados
# N:            tamanho da rede nos resultados
bc_mean_opinion <- function(Ds = seq(0.2, 0.7, by=0.1), root_dir="resultados", s_size=3, model="lattice", N=1000) {
  #m_dir <- paste(root_dir, paste(model, N, sep="_"), sep="/")
  mfile <- paste(root_dir,"mean_opinions.dat", sep="/")
  
  for (D in Ds) {
    w_dir = paste(root_dir, D, sep="/")
    
    j <- 1
    
    mean_op <- 0
    while (j <= s_size) {
      r_dir <- paste(w_dir, j, sep="/")
      opfile <- paste(r_dir, "dist.dat", sep="/")
      opinions <- load_opinions(opfile, N)
      clusters <- find_clusters(opinions)
      
      maxc <- which(clusters$pcount==max(clusters$pcount))
      split_opinions <- opinions[opinions > clusters$splits[maxc] & opinions < clusters$splits[maxc+1]]
      mean_op <- mean_op + mean(split_opinions)
      
      j <- j+1
    }
    
    mean_op <- mean_op/s_size
    
    write.table(as.matrix(t(c(D, mean_op))), 
                file=mfile, 
                row.names = FALSE, col.names = FALSE, append=TRUE)
  }
}

max_cluster_width <- function(opinions) {
  op_groups <- find_clusters(opinions)
  
  half <- max(op_groups$pcount)/2
  
  width <- 0
  n <- 1
  in_cluster <- FALSE
  max_split <- FALSE
  for (c in op_groups$pcount) {
    if (c >= half) {
      width <- width+(op_groups$splits[n+1]-op_groups$splits[n])
      in_cluster <- TRUE
      if (c == max(op_groups$pcount)) max_split <- TRUE
    }
    else if(in_cluster == TRUE){
      if (max_split == TRUE) break
      else {
        width <- 0
        in_cluster <- FALSE
      }
    }
    n <- n+1
  }
  
  return(width)
}


# Function: big_cluster_width_d
# Retorna a a largura a altura media do maior cluster de opiniao de uma rede
# datav:    vetor com os parametros
# s_size:   tamanho da amostra
# s_dir:    diretorio raiz dos resultados
# d:        dado fixo
# N:        tamanho da rede
# s_size:   tamanho da amostra
# model:    tipo da rede
big_cluster_width_d <- function(datav, root_dir, d, N=1000, s_size=3, model="lfr") {
  
  res_file <- paste(root_dir,"mean_big_cluster_width.dat", sep="/")
  
  for (i in datav) {
    if (model == "lfr") {
      w_dir <- paste(root_dir, i, paste(model, N, sep="_"), d, sep="/")
    }
    else {
      res_file <- paste(root_dir, paste(model, N, sep="_") ,"mean_big_cluster_width.dat", sep="/")
      w_dir <- paste(root_dir, paste(model, N, sep="_"), i, sep="/")
    }
   
    
    j <- 1
    mean_width <- 0
    while (j <= s_size) {
      s_dir <- paste(w_dir, j, sep="/")
      opfile <- paste(s_dir, "dist.dat", sep="/")
      opinions <- load_opinions(opfile, N)
      op_groups <- find_clusters(opinions)
      
      half <- max(op_groups$pcount)/2
      
      width <- 0
      n <- 1
      in_cluster <- FALSE
      for (c in op_groups$pcount) {
        if (c >= half) {
          width <- width+(op_groups$splits[n+1]-op_groups$splits[n])
          in_cluster <- TRUE
        }
        else if(in_cluster == TRUE){
          break
        }
        n <- n+1
      }
      
      mean_width <- mean_width + width
      j <- j+1
    }
    
    write.table(as.matrix(t(c(i, mean_width/s_size))), 
                file=res_file, 
                row.names = FALSE, col.names = FALSE, append=TRUE)
    
  }
  
  
}

# Function: consensus_time
# Salva em arquivo o tempo de consenso medio de uma amostra em funcao de
# um dado parametro
# datav:    vetor com os parametros
# s_size:   tamanho da amostra
# s_dir:    diretorio raiz dos resultados
consensus_time <- function(data_v, s_size, s_dir) {
  
  for (i in data_v) {
    w_dir = paste(s_dir, i, sep="/")
    
    j <- 1
    #mean_cons <- 0
    #error <- 0
    values <- c()
    while (j <= s_size) {
      r_dir <- paste(w_dir, j, sep="/")
      arq <- paste(r_dir, "stats.dat", sep="/")
      stats <- read.table(arq)
      #mean_cons <- mean_cons + stats[2]
      values <- append(values, unlist(stats[2]))
      write.table(as.matrix(t(c(i,stats[2]))), 
                  file=paste(s_dir, "cons_complete.dat", sep="/"), 
                  row.names = FALSE, col.names = FALSE, append=TRUE)
      j <- j+1
    }
    
    #print(values)
    #mean_cons <- mean(values)
    
    error <- sd(values)/sqrt(s_size)
    
    write.table(as.matrix(t(c(i, mean(values), error))), 
                file=paste(s_dir, "cons_mean_error.dat", sep="/"), 
                row.names = FALSE, col.names = FALSE, append=TRUE)
  }
}

# Function: consensus_time_modularity
# Salva em arquivo o tempo de consenso de uma amostra em funcao da
# modularidade
# datav:    vetor com os parametros
# s_size:   tamanho da amostra
# s_dir:    diretorio raiz dos resultados
consensus_time_modularity <- function(datav, s_size, s_dir) {
  
  for (i in datav) {
    w_dir = paste(s_dir, i, sep="/")
    
    j <- 1
    mean_cons <- 0
    mean_mod <- 0
    while (j <= s_size) {
      r_dir <- paste(w_dir, j, sep="/")
      arq <- paste(r_dir, "stats.dat", sep="/")
      stats <- read.table(arq)
      mean_mod <- mean_mod + stats[1]
      mean_cons <- mean_cons + stats[2]
      write.table(as.matrix(t(c(stats[1], stats[2]))), 
                  file=paste(s_dir, "mod_cons.dat", sep="/"), 
                  row.names = FALSE, col.names = FALSE, append=TRUE)
      j <- j+1
    }
    
    write.table(as.matrix(t(c(mean_mod/s_size, mean_cons/s_size))), 
                file=paste(s_dir, "mod_cons_mean.dat", sep="/"), 
                row.names = FALSE, col.names = FALSE, append=TRUE)
  }
}

#rdir <- "E:/work/resultados/LFR_near_k7/lfr_1000"
#consensus_time_modularity(datav=seq(1, 6, by=0.25), s_size=3, s_dir=rdir)

#rdir <- "D:/resultados/d_k_10"
#big_cluster_width_d(seq(0.1, 0.75, by=0.05), root_dir=rdir, model="er")
#big_cluster_width_d(seq(0.1, 0.75, by=0.05), root_dir=rdir, model="ba")
#big_cluster_width_d(seq(0.05, 0.95, by=0.05), d=10, root_dir=rdir, model="lfr")


#  rdir <- "E:/work/resultados/LFR_near_k7/lfr_1000"
#  consensus_time(data_v=c(3.35, 3.5, 3.6, 3.7, 3.8, 3.9,
#                           4, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8, 4.9,
#                           5, 5.1, 5.2, 5.3, 5.4, 5.5, 5.6, 5.7, 5.8, 5.9,
#                           6, 6.1, 6.2, 6.3, 6.4, 6.5, 6.6, 6.7, 6.8, 6.9,
#                           7, 7.1, 7.2, 7.3, 7.4, 7.5, 7.6, 7.7, 7.8, 7.9,
#                           8, 8.1, 8.2, 8.3, 8.4, 8.5, 8.6, 8.7, 8.8, 8.9,
#                           9, 9.5, 10), s_size=5, s_dir=rdir)
# 
# rdir <- "E:/work/resultados/LFR_near_k7_asch/lfr_1000"
# consensus_time(data_v=c(3.35, 3.5, 3.6, 3.7, 3.8, 3.9,
#                          4, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8, 4.9,
#                          5, 5.1, 5.2, 5.3, 5.4, 5.5, 5.6, 5.7, 5.8, 5.9,
#                          6, 6.1, 6.2, 6.3, 6.4, 6.5, 6.6, 6.7, 6.8, 6.9,
#                          7, 7.1, 7.2, 7.3, 7.4, 7.5, 7.6, 7.7, 7.8, 7.9,
#                          8, 8.1, 8.2, 8.3, 8.4, 8.5, 8.6, 8.7, 8.8, 8.9,
#                          9, 9.5, 10), s_size=5, s_dir=rdir)

# rdir <- "E:/work/resultados/lfr-similar-real"
# consensus_time(data_v=c("original"), s_size=5, s_dir=rdir)
library(igraph)
 rdir <- "E:/work/resultados/real_nets/e-mail2"
# consensus_time(data_v=c(0.5, 0.6), s_size=2, s_dir=rdir)
 cluster_analysis(data_v=c(0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9), root_dir=rdir, s_size=3)

#a <- runif(0, 1, 100)
#print(max_cluster_width(a))